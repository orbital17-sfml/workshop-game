# Live Update:

The file `game.cpp` tracks my local game.cpp file live as I update it on my own PC. Check it if you want to copy-paste any code I have just typed.

# Versions:

v0 to v10 serve as "checkpoints" throughout the workshop. They will be uploaded as I pass the checkpoints.

* **v0.cpp**: Display
* **v1.cpp**: Functions to Update and Draw frames
* **v2.cpp**: Basic Keyboard Input
* **v3.cpp**: Player Class
* **v4.cpp**: Gravity and Jumping
* **v5.cpp**: Bullets
* **v6.cpp**: Enemies
* **v7.cpp**: Collision Detection
* **v8.cpp**: Enemies that can attack you (by shooting bullets)
* **v9.cpp**: Platforms
* **v10.cpp**: Restart Button